import os
import sys
from argparse import (
    ArgumentParser,
)
from shlex import (
    split,
)
from subprocess import (  # noqa: S404 Consider possible security implications associated with PIPE module.
    PIPE,
    run,
)
from typing import (
    Any,
    Optional,
)


def run_command(command_line: str, *args: Any, **kwargs: Any) -> str:
    return run(  # type: ignore # noqa: S603 subprocess call - check for execution of untrusted input.
        split(command_line),
        universal_newlines=True,
        check=True,
        stdout=PIPE,
        *args,
        **kwargs,
    ).stdout


branch_name = run_command("git branch --show-current")
user_name = run_command("git branch --show-current")


def file_content(
    description: str,
    entry_type: str,
    user_name: str,
    user_email: str,
    mr_id: Optional[int],
) -> str:
    return (
        f'description = "{description}"\n'
        f'type = "{entry_type}"\n\n'
        "[author]\n"
        f'name = "{user_name}"\n'
        f'email = "{user_email}"\n'
        + (
            "\n[origin]\n"
            f'merge_request = "{{ cookiecutter.repository_url }}/-/merge_requests/{mr_id}"\n'
            if mr_id
            else ""
        )
    )


def read_int(min_value: int, max_value: Optional[int]) -> int:
    while True:
        try:
            value = int(input())
            if value < min_value or (max_value and value > max_value):
                raise Exception()
            else:
                break
        except Exception:
            print(
                f"Invalid input. Should be an index in range {min_value}...{max_value if max_value else ''}"
            )
        except KeyboardInterrupt:
            raise SystemExit(0)
    return value


def main() -> int:
    types = ["Added", "Changed", "Deprecated", "Removed", "Fixed", "Security"]

    parser = ArgumentParser("make-changelog-entry")

    parser.add_argument(
        "--version", default="unreleased", help="For which version is the entry"
    )
    parser.add_argument("--type", choices=types, help="Type of entry")
    parser.add_argument(
        "--no-merge-request",
        action="store_true",
        help="If specified, no merge request is asked",
    )
    parser.add_argument("--merge-request", type=int, help="Merge request id")
    parser.add_argument("--description", default="", help="Description of change")
    parser.add_argument(
        "--no-edit",
        action="store_true",
        help="If specified don't try to open the editor",
    )
    args = parser.parse_args()

    branch_name = run_command("git branch --show-current").strip()
    parent_dir = f"changelog/{args.version}"
    file_path = f"{parent_dir}/{branch_name}.toml"

    if os.path.exists(file_path):
        print(f"{file_path} already exists.", file=sys.stderr)
        return 1

    if args.type:
        entry_type = args.type
    else:
        print(
            "Choose type:\n\t" + "".join((f"{i}. {t}\n\t" for i, t in enumerate(types)))
        )
        entry_type = types[read_int(0, len(types) - 1)]

    mr_id = None
    if not args.no_merge_request:
        if args.merge_request:
            mr_id = args.merge_request
        else:
            print("What is the merge request id ?")
            mr_id = read_int(1, None)

    # Future: should be customizable with .changelogrc in home dir or project dir
    user_name = run_command("git config --get user.name").strip()
    user_email = run_command("git config --get user.email").strip()

    if not os.path.exists(parent_dir):
        os.mkdir(parent_dir)

    with open(file_path, "w") as f:
        f.write(
            file_content(args.description, entry_type, user_name, user_email, mr_id)
        )

    if not args.no_edit:
        editor = os.environ.get("EDITOR")
        if editor:
            run_command(f"env {editor} {file_path}", shell=True)  # noqa: S604

    return 0


if __name__ == "__main__":
    exit(main())
