import os
import tempfile
from contextlib import (
    contextmanager,
)
from typing import (
    Any,
    Generator,
)

import nox
from nox.sessions import (
    Session,
)

nox.options.sessions = "lint", "mypy", "safety", "tests"

SOURCE_LOCATIONS = ["src", "tests", "infra", "noxfile.py"]


@contextmanager
def temporary_file() -> Generator[str, None, None]:
    # On windows we cannot use tempfile.NamedTemporaryFile() directly because
    # the file cannot be written while still open
    tmpf = tempfile.NamedTemporaryFile(delete=False)
    tmpf.close()
    try:
        yield tmpf.name
    finally:
        os.unlink(tmpf.name)


def export_poetry_requirements(session: Session, file_name: str) -> None:
    session.run(
        "poetry",
        "export",
        "--dev",
        "--format=requirements.txt",
        "--without-hashes",
        f"--output={file_name}",
        external=True,
    )


def install_with_constraints(session: Session, *args: str, **kwargs: Any) -> None:
    with temporary_file() as requirements:
        export_poetry_requirements(session, requirements)
        session.install(f"--constraint={requirements}", *args, **kwargs)


@nox.session(python=["{{ '.'.join(cookiecutter.python_version.split('.')[0:2]) }}"])
def tests(session: Session) -> None:
    args = session.posargs or [
        "--cov-fail-under",
        "100",
        "-m" "not e2e",
    ]
    session.run("poetry", "install", "--no-dev", external=True)
    install_with_constraints(
        session, "coverage[toml]", "pytest", "pytest-cov", "pytest-mock", "typeguard"
    )
    session.run("pytest", *args)


@nox.session(python=["{{ '.'.join(cookiecutter.python_version.split('.')[0:2]) }}"])
def lint(session: Session) -> None:
    args = session.posargs or SOURCE_LOCATIONS{% raw %}
    install_with_constraints(
        session,
        {{lint_dependencies}}
    ){% endraw %}
    session.run("flake8", *args)


@nox.session(python=["{{ '.'.join(cookiecutter.python_version.split('.')[0:2]) }}"])
def safety(session: Session) -> None:
    with temporary_file() as requirements:
        export_poetry_requirements(session, requirements)
        session.install("safety")
        session.run("safety", "check", f"--file={requirements}", "--full-report")


@nox.session(python=["{{ '.'.join(cookiecutter.python_version.split('.')[0:2]) }}"])
def mypy(session: Session) -> None:
    args = session.posargs or SOURCE_LOCATIONS
    install_with_constraints(session, "mypy")
    session.run("mypy", *args)


@nox.session
def black(session: Session) -> None:
    args = session.posargs or SOURCE_LOCATIONS
    install_with_constraints(session, "black")
    session.run("black", *args)
