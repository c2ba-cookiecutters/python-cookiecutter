"""Console script for {{cookiecutter.project_slug}}."""

import typer

from {{ cookiecutter.package_name }} import (
    __version__,
)

app = typer.Typer()


def version_callback(value: bool) -> None:
    if value:
        typer.echo(f"{{cookiecutter.project_name}} version {__version__}")
        raise typer.Exit()


@app.command()
def main(
    version: bool = typer.Option(False, "--version", callback=version_callback),
) -> None:
    """Console script for {{cookiecutter.project_slug}}."""


if __name__ == "__main__":  # pragma: no cover
    app()
