"""
link.py - Create a symlink from '.vscode' to a config folder from this
repository.
"""
import argparse
import os
import shutil
import sys
from pathlib import (
    Path,
)


def main() -> int:
    parser = argparse.ArgumentParser(description="Link '.vscode' to a config.")
    parser.add_argument(
        "config_name",
        nargs="?",
        type=str,
        default="default",
        help="Name of the configuration to link.",
    )
    args = parser.parse_args()

    vscode_git_dir = os.path.dirname(os.path.abspath(__file__))
    config_path = os.path.join(vscode_git_dir, args.config_name)

    if not os.path.exists(config_path):
        print(
            f"{config_path} does not exist: I will copy 'default' config as "
            f"'{args.config_name}' and link to it."
        )
        default_config_path = os.path.join(vscode_git_dir, "default")
        if not os.path.isdir(default_config_path):
            print(
                "'default' config has been removed or is not a directory "
                "anymore. Please create your config manually."
            )
            return 1
        shutil.copytree(default_config_path, config_path)

    if not os.path.isdir(config_path):
        print(f"{config_path} should be a directory.")
        return 1

    config_link_path = str(Path(vscode_git_dir).parent / ".vscode")

    if os.path.exists(config_link_path):
        print(f"Unlinking {config_link_path}.")
        os.unlink(config_link_path)

    print(f"Linking {config_link_path} -> {config_path}.")
    if sys.platform == "win32":
        import _winapi

        _winapi.CreateJunction(config_path, config_link_path)
    else:
        os.symlink(config_path, config_link_path)

    return 0


if __name__ == "__main__":
    exit(main())
