import sys


def main() -> int:
    open_source_license = "{{ cookiecutter.open_source_license }}"
    license_copyright_name = "{{ cookiecutter.license_copyright_name }}"

    if (
        open_source_license in ["MIT", "BSD", "ISC", "Apache", "GPLv3"]
        and license_copyright_name == ""
    ):
        print(
            f"License {open_source_license} requires a copyright name.", file=sys.stderr
        )
        return 1

    return 0


if __name__ == "__main__":
    sys.exit(main())
