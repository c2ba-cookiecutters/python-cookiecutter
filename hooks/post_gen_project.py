import sys
import os
import subprocess
import shlex
from jinja2 import Template
from typing import IO

DEV_DEPENDENCIES_DICT = {}

DEV_DEPENDENCIES_DICT["formatting"] = [
    "black",
    "isort",
]

DEV_DEPENDENCIES_DICT["testing"] = [
    "coverage[toml]",
    "pytest",
    "pytest-cov",
    "pytest-mock",
]

# Where to find flake8 extensions: https://github.com/DmytroLitvinov/awesome-flake8-extensions
DEV_DEPENDENCIES_DICT["linting"] = [
    "flake8",
    "flake8-absolute-import",
    "flake8-annotations",  # Flake8 Type Annotation Checking
    "flake8-bandit",  # Automated security testing using bandit and flake8.
    "flake8-black",
    "flake8-bugbear",  # A plugin for Flake8 finding likely bugs and design problems in your program.
    "flake8-builtins",  # Check for python builtins being used as variables or parameters
    "flake8-comprehensions",  # A flake8 plugin to help you write better list/set/dict comprehensions.
    # "flake8-docstrings",  # A plugin to flake8 to include checks provided by pep257
    "flake8-eradicate",  # Flake8 plugin to find commented out or dead code
    "flake8-isort",
    "flake8-pytest-style",  # A flake8 plugin checking common style issues or inconsistencies with pytest-based tests.
    "pep8-naming",  # Naming Convention checker for Python
]

DEV_DEPENDENCIES_DICT["security"] = [
    "safety",
]

DEV_DEPENDENCIES_DICT["typing"] = [
    "mypy",
    "typeguard",
]

DEV_DEPENDENCIES_DICT["automation"] = [
    "nox",
    "pre-commit",
]

DEV_DEPENDENCIES = []

for key, value in DEV_DEPENDENCIES_DICT.items():
    DEV_DEPENDENCIES.extend(value)


def run(cmd_args: str, check=True, *args, **kwargs):
    try:
        return subprocess.run(
            shlex.split(cmd_args), check=check, universal_newlines=True, *args, **kwargs
        )
    except Exception:
        print(f"Failed to run command: {cmd_args}", file=sys.stderr)
        raise


def run_python_app(python_exe: str, cmd_args: str, *args, **kwargs):
    return run(f"{python_exe} -m {cmd_args}", *args, **kwargs)


def git(cmd_args: str, *args, **kwargs):
    return run(f"git {cmd_args}", *args, **kwargs)


def main() -> int:
    open_source_license = "{{ cookiecutter.open_source_license }}"
    if open_source_license == "None":
        os.remove("LICENSE")

    with_command_line_interface = (
        "{{ cookiecutter.with_command_line_interface }}" == "yes"
    )
    if not with_command_line_interface:
        os.remove("src/{{cookiecutter.package_name}}/console.py")
        os.remove("tests/test_console.py")

    with open("noxfile.py", "r+") as noxfile:
        template = Template(noxfile.read())
        new_noxfile_content = template.render(
            lint_dependencies=",\n        ".join(
                (f'"{x}"' for x in DEV_DEPENDENCIES_DICT["linting"])
            )
            + ","
        )
        noxfile.seek(0)
        noxfile.write(new_noxfile_content)
    with open("noxfile.py", "rb+") as noxfile:
        fix_end_of_file(noxfile)

    project_desc = "{{ cookiecutter.project_description }}"
    package_name = "{{ cookiecutter.package_name }}"
    author_name = "{{ cookiecutter.author_name }}"
    author_email = "{{ cookiecutter.author_email }}"
    poetry_author = author_name
    if author_email != "":
        poetry_author = f"{poetry_author} <{author_email}>"

    poetry_version = run(f"env poetry --version", stdout=subprocess.PIPE).stdout
    print(f"Using {poetry_version}")

    dependencies = []

    if with_command_line_interface:
        dependencies.append("typer")

    print("Creating poetry project...")
    run(
        f'env poetry init --no-interaction --name "{package_name}" '
        + (f'--description "{project_desc}" ' if project_desc != "" else "")
        + (f'--author "{poetry_author}" ' if poetry_author != "" else "")
        + (
            f'--license "{open_source_license}" '
            if open_source_license != "None"
            else ""
        )
        + '--python "^{{ cookiecutter.python_version }}" '
        + " ".join(f'--dependency "{dep}"' for dep in dependencies)
        + " "
        + " ".join((f'--dev-dependency "{dep}"' for dep in DEV_DEPENDENCIES))
    )

    # Init before bootstrap because it installs the pre-commit, so it needs a git repository
    git("init")

    run("env poetry lock")

    if open_source_license != "None":
        with open("LICENSE", "rb+") as file_obj:
            ret_for_file = fix_end_of_file(file_obj)
            if ret_for_file:
                print("Fixing LICENSE")

    git("add .")

    if "{{ cookiecutter.run_bootstrap }}" == "yes":
        run("env bash bootstrap.sh")
        run(
            "env poetry run pre-commit run -a",
            check=False,
        )  # Run pre-commit to fix bad formatted files after generation
        git("add .")

    git('commit -m "Initial commit with project template."')
    version_string = "0.1.0"
    git(f"tag v{version_string}")
    run(
        f"python infra/make-changelog-entry.py "
        "--type Added "
        "--no-merge-request "
        '--description "Creation of the project" '
        "--version {version_string}"
    )

    remote_url = "{{ cookiecutter.repository_url }}"
    if remote_url != "":
        git(f"remote add origin {remote_url}")

    run(f"python .vscode-git/link.py {{ cookiecutter.vscode_config_name }}")

    return 0


def fix_end_of_file(file_obj: IO[bytes]) -> int:
    # Copyright (c) 2014 pre-commit dev team: Anthony Sottile, Ken Struys
    # https://github.com/pre-commit/pre-commit-hooks/blob/master/LICENSE
    # Test for newline at end of file
    # Empty files will throw IOError here
    try:
        file_obj.seek(-1, os.SEEK_END)
    except OSError:
        return 0
    last_character = file_obj.read(1)
    # last_character will be '' for an empty file
    if last_character not in {b"\n", b"\r"} and last_character != b"":
        # Needs this seek for windows, otherwise IOError
        file_obj.seek(0, os.SEEK_END)
        file_obj.write(b"\n")
        return 1

    while last_character in {b"\n", b"\r"}:
        # Deal with the beginning of the file
        if file_obj.tell() == 1:
            # If we've reached the beginning of the file and it is all
            # linebreaks then we can make this file empty
            file_obj.seek(0)
            file_obj.truncate()
            return 1

        # Go back two bytes and read a character
        file_obj.seek(-2, os.SEEK_CUR)
        last_character = file_obj.read(1)

    # Our current position is at the end of the file just before any amount of
    # newlines.  If we find extraneous newlines, then backtrack and trim them.
    position = file_obj.tell()
    remaining = file_obj.read()
    for sequence in (b"\n", b"\r\n", b"\r"):
        if remaining == sequence:
            return 0
        elif remaining.startswith(sequence):
            file_obj.seek(position + len(sequence))
            file_obj.truncate()
            return 1

    return 0


if __name__ == "__main__":
    sys.exit(main())
