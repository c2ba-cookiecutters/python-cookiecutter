# Cookiebutter for a Python project

## Description

This is a [cookiecutter](https://cookiecutter.readthedocs.io/) to create a new Python project matching several conventions. It is based on the [Hypermodern Python tutorial](https://cjolowicz.github.io/posts/hypermodern-python-01-setup/).

You need cookiecutter and poetry installed and accessible from your PATH.

## Features

- poetry based project, with `src` layout

## Summary of [Hypermodern Python tutorial](https://cjolowicz.github.io/posts/hypermodern-python-01-setup/)

Here is a quick summary of the main points adressed by the tutorial that are integrated in this cookiecutter.

## Todo

### Changelog generation

- Use a list of files under a folder changelog ? with a deterministic algorithm.
- Use git log vXXX...vYYY --pretty ?
  - https://jerel.co/blog/2011/07/generating-a-project-changelog-using-git-log
  - https://coderwall.com/p/5cv5lg/generate-your-changelogs-with-git-log

Example with commits and a changelog tag:

```
git log --pretty=format:'<li> <a href="http://github.com/jerel/project/commit/%H">view commit &bull;</a> %s</li> ' | grep "#changelog" | sed 's/ #changelog//g'
```

### isort

One import per line (break) to reduce merge conflicts.

### At the end: rewrite master history with logical steps

- Add poetry
- Add flake config
- ...

Each step should work (generate a valid project)

### Tooling for gitlab release

### Cleanup nox jobs

### Configure python versions to test
